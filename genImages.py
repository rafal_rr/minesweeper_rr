import png
import os

tuple14 = (255,255,0, 255,255,0, 255,255,0, 255,255,0, 255,255,0, 255,255,0, 255,255,0, 255,255,0, 255,255,0, 255,255,0, 255,255,0, 255,255,0, 255,255,0, 255,255,0)
assert(14*3 == len(tuple14))

for genFile in os.listdir("images"):
    if ".gen" != os.path.splitext(genFile)[1]:
        continue
    print(genFile)
    readMeta = True
    imageWidth = None
    imageHeight = None
    metaDictionary = {}
    metaDictionary[" "] = (255,255,0)
    rgbList = []
    for line in open(os.path.join("images", genFile)).readlines():
        if '\n' == line:
            readMeta = False
            continue
        if imageWidth is None:
            sizeData = line.split()
            assert(2 == len(sizeData))
            imageWidth = int(sizeData[0])
            imageHeight = int(sizeData[1])
            print("Size: " + str(imageWidth) + "x" + str(imageHeight))
            continue
        if readMeta:
            metaData = line.split()
            assert(4 == len(metaData))
            metaDictionary[metaData[0]] = (int(metaData[1], 16), int(metaData[2], 16), int(metaData[3], 16))
            print("'" + metaData[0] + "': " + str(metaDictionary[metaData[0]]))
        else:
            line = line.replace("\n", "")
            assert(imageWidth == len(line))
            tupleList = map(lambda c: metaDictionary[c], line)
            rgbTuple = tuple(i for sub in tupleList for i in sub)
            assert(imageWidth*3 == len(rgbTuple))
            rgbList.append(rgbTuple)
    # print(len(rgbList))
    assert(imageHeight == len(rgbList))
    f = open(os.path.join("images", os.path.splitext(genFile)[0] + ".png"), 'wb')
    w = png.Writer(imageWidth, imageHeight, transparent=(255, 255, 0))
    w.write(f, rgbList)
    f.close()

