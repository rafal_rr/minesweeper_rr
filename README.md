# Minesweeper_rr #

This is a clone of classic logic game "Minesweeper" well known from Windows OSs.
[Play it on my website](http://rafalrutkowski.pl/minesweeper_rr/index.html)

### Rules and strategy ###

* You can learn it for example [here](http://www.minesweeper.info/wiki/Strategy)

### Contribution ###

* If you have some ideas and want to join the repo, contact with me:
* Author: [rafal_rr](https://bitbucket.org/rafal_rr/)


# Enjoy playing the game! #