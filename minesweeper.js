
var underDevelopment_ = true;

function assert(condition, message, callback)
{
    if (!condition)
    {
        console.error(message);
        console.trace();
        if (callback)
        {
            callback();
        }
    }
}

var Controller = function(model, view, logger)
{
    this.model_ = model;
    this.view_ = view;
    this.logger_ = logger;
    this.logger_.debug('Controller created');
}

Controller.prototype.fetchGetData = function()
{
    this.getData_ = {};
    args = location.search.substr(1).split(/&/);
    for (var i=0; i<args.length; ++i)
    {
        var tmp = args[i].split(/=/);
        if (tmp[0] == "")
        {
            continue;
        }
        this.getData_[decodeURIComponent(tmp[0])] = decodeURIComponent(tmp.slice(1).join("").replace("+", " "));
    }
    if (!('w' in this.getData_))
    {
        this.getData_['w'] = this.model_.defaultW;
    }
    if (!('h' in this.getData_))
    {
        this.getData_['h'] = this.model_.defaultH;
    }
    if (!('mines' in this.getData_))
    {
        this.getData_['mines'] = this.model_.countDefaultMines(this.getData_['w'], this.getData_['h']);
    }
}

Controller.prototype.init = function()
{
    this.logger_.debug("Controller init");
    this.fetchGetData();
    this.logger_.debug(this.getData_);
    this.model_.init(
        this.getData_['w'],
        this.getData_['h'],
        this.getData_['mines']);
    if (this.model_.modelInvalid_)
    {
        // TODO
    }
    this.view_.init();
    this.view_.refresh();
}

Controller.prototype.startGame = function(startingPoint)
{
    this.logger_.debug("Starting a game");
    this.model_.startGame(startingPoint);
    this.view_.refresh();
}

Controller.prototype.chooseField = function(x, y)
{
    this.logger_.log("Field [", x, ", ", y, "] chosen");
    if (!this.model_.onGoing_)
    {
        if (this.model_.won_ || this.model_.gameOver_)
        {
            this.logger_.log("Ignore");
            return;
        }
        this.startGame([x, y]);
    }
    fieldsToRefresh = this.model_.uncover(x, y);
    // this.logger_.debug("fieldsToRefresh: ", fieldsToRefresh);
    this.view_.refresh(fieldsToRefresh);
}

Controller.prototype.switchFlagIfPossible = function(x, y)
{
    this.logger_.log("Try to switch flag: [", x, ", ", y, "]");
    if (!this.model_.onGoing_)
    {
        this.logger_.log("Ignore");
        return;
    }
    this.model_.switchFlagIfPossible(x, y);
    this.view_.refresh([[x, y]]);
}

Controller.prototype.onTimerTick = function()
{
    this.model_.onTimerTick();
    this.view_.refresh();
}

var Model = function(logger)
{
    this.logger_ = logger;
    this.defaultW = 8;
    this.defaultH = 8;
    this.CODE_MINE = '*';
    this.CODE_MINE_HIDDEN = '#';
    this.CODE_MINE_HIDDEN_FLAGGED = '@';
    this.CODE_HIDE_OFFSET = 16;
    this.CODE_FLAG_OFFSET = 32;
    this.logger_.info('Model created');
}

Model.prototype.countDefaultMines = function(w, h)
{
    return Math.floor(w*8 / 6,4);
}

Model.prototype.init = function(w, h, mines)
{
    this.w_ = parseInt(w);
    this.h_ = parseInt(h);
    this.mines_ = parseInt(mines);

    this.modelInvalid_ = false;
    var setInvalid = function(self){return function(){self.modelInvalid_ = true;}}(this);
    assert(this.w_ > 0, "w<=0", setInvalid);
    assert(this.h_ > 0, "h<=0", setInvalid);
    assert(this.mines_ > 0, "Too less mines!", setInvalid);
    assert(this.w_ * this.h_ > this.mines_, "Too much mines!", setInvalid);
    if (this.modelInvalid_)
    {
        this.logger_.error("Invalid data");
        return;
    }
    this.logger_.debug("Model init");
    this.initPoints();
}

Model.prototype.createBoard = function(startingPoint)
{
    assert(!this.modelInvalid_);

    // create empty board
    this.board_ = [];
    for (var ix = 0; ix < this.w_; ++ix)
    {
        this.board_[ix] = [];
        for (var iy = 0; iy < this.h_; ++iy)
        {
            this.board_[ix][iy] = 0;
        }
    }
    // this.logBoardToConsole("Empty board");

    // put mines in random places
    for (var i = 0; i < this.mines_; ++i)
    {
        do
        {
            rx = Math.floor(Math.random() * this.w_);
            ry = Math.floor(Math.random() * this.h_);
        }
        while (this.CODE_MINE == this.board_[rx][ry] || (rx == startingPoint[0] && ry == startingPoint[1]));
        this.logger_.debug("random: ", rx, ry);
        this.board_[rx][ry] = this.CODE_MINE;
    }
    // put mines neighbourhood numbers
    for (var ix = 0; ix < this.w_; ++ix)
    {
        for (var iy = 0; iy < this.h_; ++iy)
        {
            if (this.CODE_MINE == this.board_[ix][iy])
            {
                continue;
            }
            this.countNeighbourhood(ix, iy);
        }
    }
    this.logBoardToConsole("Board with mines");

    // hide a board
    for (var ix = 0; ix < this.w_; ++ix)
    {
        for (var iy = 0; iy < this.h_; ++iy)
        {
            this.hideField(ix, iy);
        }
    }
    // this.logBoardToConsole("Hidden board with mines");
}

Model.prototype.countNeighbourhood = function(x, y)
{
    assert(!this.modelInvalid_);
    if (this.CODE_MINE == this.board_[x][y])
    {
        this.logger_.warning("countNeighbourhood called for non-empty field!", x, y, this.board_[x][y]);
        return;
    }
    this.board_[x][y] = 0;
    for (var ix = x-1; ix <= x+1; ++ix)
    {
        if (ix <0 || ix >= this.w_)
        {
            continue;
        }
        for (var iy = y-1; iy <= y+1; ++iy)
        {
            if (iy <0 || iy >= this.h_)
            {
                continue;
            }
            if (ix == x && iy == y)
            {
                continue;
            }
            if (this.CODE_MINE == this.board_[ix][iy])
            {
                this.board_[x][y]++;
            }
        }
    }
}

Model.prototype.logBoardToConsole = function(message)
{
    assert(!this.modelInvalid_);
    if (!underDevelopment_)
    {
        return;
    }
    this.logger_.debug(message + ": ");
    for (var iy = 0; iy < this.h_; ++iy)
    {
        row = [];
        for (var ix = 0; ix < this.w_; ++ix)
        {
            row.push(this.board_[ix][iy]);
        }
        this.logger_.debug(row);
    }
}

Model.prototype.hideField = function(x, y)
{
    assert(!this.modelInvalid_);
    if (this.isHidden(x, y))
    {
        return;
    }
    if (this.CODE_MINE == this.board_[x][y])
    {
        this.board_[x][y] = this.CODE_MINE_HIDDEN;
    }
    else
    {
        this.board_[x][y] += this.CODE_HIDE_OFFSET;
    }
}

Model.prototype.uncoverField = function(x, y)
{
    assert(!this.modelInvalid_);
    assert(this.onGoing_);
    if (!this.isHidden(x, y))
    {
        return;
    }
    if (this.isFlagged(x, y))
    {
        this.logger_.debug(x, y, " flagged");
        return;
    }
    if (this.CODE_MINE_HIDDEN == this.board_[x][y])
    {
        this.board_[x][y] = this.CODE_MINE;
        this.gameOver_ = true;
        this.fieldOfDeath_ = [x, y];
        this.onGoing_ = false;
    }
    else
    {
        this.board_[x][y] -= this.CODE_HIDE_OFFSET;
        this.uncoveredFields_ -= 1;
        if (0 == this.uncoveredFields_)
        {
            this.onGoing_ = false;
            this.won_ = true;
        }
    }
}

Model.prototype.isHidden = function(x, y)
{
    assert(!this.modelInvalid_);
    return this.isFlagged(x, y)
        || this.CODE_MINE_HIDDEN == this.board_[x][y]
        || this.board_[x][y] >= this.CODE_HIDE_OFFSET;
}

Model.prototype.flagField = function(x, y)
{
    assert(!this.modelInvalid_);
    assert(this.isHidden(x, y));
    assert(!this.isFlagged(x, y));
    if (this.CODE_MINE_HIDDEN == this.board_[x][y])
    {
        this.board_[x][y] = this.CODE_MINE_HIDDEN_FLAGGED;
    }
    else
    {
        this.board_[x][y] += this.CODE_FLAG_OFFSET;
    }
}

Model.prototype.unflagField = function(x, y)
{
    assert(!this.modelInvalid_);
    assert(this.isHidden(x, y));
    assert(this.isFlagged(x, y));
    if (this.CODE_MINE_HIDDEN_FLAGGED == this.board_[x][y])
    {
        this.board_[x][y] = this.CODE_MINE_HIDDEN;
    }
    else
    {
        this.board_[x][y] -= this.CODE_FLAG_OFFSET;
    }
}

Model.prototype.isFlagged = function(x, y)
{
    assert(!this.modelInvalid_);
    return this.CODE_MINE_HIDDEN_FLAGGED == this.board_[x][y] || this.board_[x][y] >= this.CODE_FLAG_OFFSET;
}

Model.prototype.initPoints = function()
{
    assert(!this.modelInvalid_);
    this.flagsLeft_ = this.mines_;
    this.uncoveredFields_ = this.w_ * this.h_ - this.mines_;
    this.time_ = 0;
    this.onGoing_ = false;
    this.gameOver_ = false;
    this.fieldOfDeath_ = null;
    this.won_ = false;
}

Model.prototype.startGame = function(startingPoint)
{
    assert(!this.onGoing_);
    assert(!this.gameOver_);
    assert(!this.won_);
    this.createBoard(startingPoint);
    this.time_ = 0;
    this.onGoing_ = true;
}

Model.prototype.uncover = function(x, y)
{
    if (!this.isHidden(x, y))
    {
        return [];
    }
    if (this.gameOver_)
    {
        return;
    }
    if (!this.onGoing_)
    {
        this.onGoing_ = true;
    }
    var result = [[x, y]];
    var rest = [[x, y]];
    do
    {
        var xy = rest.shift();
        this.uncoverField(xy[0], xy[1]);
        if (0 == this.board_[xy[0]][xy[1]])
        {
            neighbours = this.getHiddenNeighboursOf(xy[0], xy[1]);
            result = result.concat(neighbours);
            rest = rest.concat(neighbours);
        }
    }
    while (0 < rest.length);
    if (this.gameOver_ || this.won_)
    {
        otherMines = this.getAllMinesPostmortem();
        this.logger_.debug("l: ", otherMines.length);
        this.logger_.debug("otherMines: ", otherMines);
        for (var i = 0; i < otherMines.length; ++i)
        {
            this.logger_.debug("Uncover postmortem: ", otherMines[i]);
            this.uncoverMinePostmortem(otherMines[i][0], otherMines[i][1]);
        }
        result = result.concat(otherMines);
    }
    return result;
}

Model.prototype.getHiddenNeighboursOf = function(x, y)
{
    var result = [];
    for (var ix = x-1; ix <= x+1; ++ix)
    {
        if (ix <0 || ix >= this.w_)
        {
            continue;
        }
        for (var iy = y-1; iy <= y+1; ++iy)
        {
            if (iy <0 || iy >= this.h_)
            {
                continue;
            }
            if (ix == x && iy == y)
            {
                continue;
            }
            if (!this.isHidden(ix, iy))
            {
                continue;
            }
            result.push([ix, iy]);
        }
    }
    return result;
}

Model.prototype.getAllMinesPostmortem = function()
{
    var result = [];
    for (var ix = 0; ix < this.w_; ++ix)
    {
        for (var iy = 0; iy < this.h_; ++iy)
        {
            if (this.isMine(ix, iy) && this.isHidden(ix, iy))
            {
                result.push([ix, iy]);
            }
        }
    }
    return result;
}

Model.prototype.uncoverMinePostmortem = function(x, y)
{
    assert(this.isHidden(x, y), this.board_[x][y] + ": is not hidden: " + x + ", " + y);
    this.board_[x][y] = this.CODE_MINE;
}

Model.prototype.isMine = function(x, y)
{
    return this.CODE_MINE == this.board_[x][y]
        || this.CODE_MINE_HIDDEN == this.board_[x][y]
        || this.CODE_MINE_HIDDEN_FLAGGED == this.board_[x][y];
}

Model.prototype.switchFlagIfPossible = function(x, y)
{
    if (!this.isHidden(x, y))
    {
        return;
    }
    if (this.isFlagged(x, y))
    {
        this.logger_.debug("UNFLAG");
        this.unflagField(x, y);
        this.flagsLeft_ += 1;
    }
    else
    {
        if (this.flagsLeft_ == 0)
        {
            return;
        }
        this.logger_.debug("FLAG");
        this.flagField(x, y);
        this.flagsLeft_ -= 1;
    }
}

Model.prototype.onTimerTick = function()
{
    if (!this.onGoing_)
    {
        return;
    }
    this.time_++;
}

var View = function(model, debugMode, logger)
{
    this.model_ = model;
    this.debugMode_ = debugMode;
    this.logger_ = logger;
}

View.prototype.init = function()
{
    if (this.model_.modelInvalid_)
    {
        return;
    }
    html = "<p class=\"statistics\"><img src=\"images/flag.png\"/>: <span id=\"game_flags_left\"></span> / <span id=\"game_mines\"></span>; ";
    html += "<img src=\"images/spade.png\"/>: <span id=\"game_fields_uncovered\"></span> / " + (this.model_.w_*this.model_.h_-this.model_.mines_) + "; ";
    html += "<img src=\"images/clock.png\"/>: <span id=\"game_time\"></span>s</p>";
    if (this.debugMode_)
    {
        html += "<div id=\"game_state\">NOT STARTED</div>";
    }
    html += "<div id=\"game_start\"></div>";
    html += this.printBoard();
    this.getGameDiv().innerHTML = html;
    for (var iy = 0; iy < this.model_.h_; ++iy)
    {
        for (var ix = 0; ix < this.model_.w_; ++ix)
        {
            id = "game_field_" + ix + "_" + iy;
            document.getElementById(id).addEventListener(
                "mousedown",
                function(x, y)
                {
                    return function(e)
                    {
                        onMouseDownNew(e, x, y);
                    };
                }(ix, iy));
        }
    }
}

View.prototype.refresh = function(fieldsToRefresh)
{
    if (this.model_.modelInvalid_)
    {
        this.printInvalidModelMessage();
        return;
    }
    document.getElementById("game_flags_left").innerHTML = this.model_.flagsLeft_;
    document.getElementById("game_mines").innerHTML = this.model_.mines_;
    document.getElementById("game_fields_uncovered").innerHTML = this.model_.uncoveredFields_;
    document.getElementById("game_time").innerHTML = this.model_.time_;
    if (this.model_.onGoing_ || this.model_.won_ || this.model_.gameOver_)
    {
        document.getElementById("game_start").innerHTML
            = "<button onclick=\"controller.init();\">RESTART</button>";
        if (this.debugMode_)
        {
            if (this.model_.gameOver_)
            {
                document.getElementById("game_state").innerHTML = "GAME OVER";
            }
            else if (this.model_.won_)
            {
                document.getElementById("game_state").innerHTML = "VICTORY";
            }
            else
            {
                document.getElementById("game_state").innerHTML = "ONGOING";
            }
        }
    }
    else
    {
        document.getElementById("game_start").innerHTML
            = "<button onclick=\"controller.init();\">INIT</button>";
        if (this.debugMode_)
        {
            document.getElementById("game_state").innerHTML = "NOT STARTED";
        }
    }
    this.refreshBoard(fieldsToRefresh);
    this.logger_.debug("View refreshed");
}

View.prototype.printInvalidModelMessage = function()
{
    this.getGameDiv().innerHTML = "<h1>ERROR</h1>";
}

View.prototype.getGameDiv = function()
{
    return document.getElementById("game_div");
}

View.prototype.printBoard = function()
{
    boardHtml = "<table class=\"game_table\">";
    for (var iy = 0; iy < this.model_.h_; ++iy)
    {
        boardHtml += "<tr>";
        for (var ix = 0; ix < this.model_.w_; ++ix)
        {
            id = "game_field_" + ix + "_" + iy;
            boardHtml += "<td class=\"game_td\">";
            var onContextMenuString
                = "oncontextmenu=\"return false\"";
            boardHtml += "<div class=\"game_field_covered\" id=\"" + id + "\""
                + onContextMenuString + "></div>";
            boardHtml += "</td>";
        }
        boardHtml += "</tr>";
    }
    boardHtml += "</table>";

    return boardHtml;
}

View.prototype.refreshBoard = function(fieldsToRefresh)
{
    if (!fieldsToRefresh || 0 == fieldsToRefresh.length)
    {
        this.logger_.debug("No fields to refresh");
        return;
    }
    this.logger_.log("refreshBoard, fieldOfDeath_: ", this.model_.fieldOfDeath_);
    for (var i = 0; i < fieldsToRefresh.length; ++i)
    {
        var x = fieldsToRefresh[i][0];
        var y = fieldsToRefresh[i][1];
        id = "game_field_" + x + "_" + y;
        var fieldDiv = document.getElementById(id);
        if (this.model_.isHidden(x, y))
        {
            fieldDiv.className = "game_field_covered";
            if (this.model_.isFlagged(x, y))
            {
                fieldDiv.innerHTML = "<span class=\"helper\"></span><img src=\"images/flag.png\"/>";
            }
            else
            {
                fieldDiv.innerHTML = "";
            }
            continue;
        }
        else if (null != this.model_.fieldOfDeath_
            && x == this.model_.fieldOfDeath_[0]
            && y == this.model_.fieldOfDeath_[1])
        {
            fieldDiv.className = "game_field_of_death";
        }
        else if (this.model_.won_ && this.model_.board_[x][y] == "*")
        {
            fieldDiv.className = "game_field_mine_after_win";
        }
        else
        {
            fieldDiv.className = "game_field_uncovered";
        }

        if (this.model_.board_[x][y] == "*")
        {
            fieldDiv.innerHTML = "<span class=\"helper\"></span><img src=\"images/mine.png\"/>";
        }
        else if (this.model_.board_[x][y] != 0)
        {
            fieldDiv.innerHTML = "<span class=\"helper\"></span><img src=\"images/" + this.model_.board_[x][y] + ".png\"/>";
        }
    }
}

View.prototype.refreshAllBoard = function()
{
    var fieldsToRefresh = [];
    for (var ix = 0; ix < this.model_.w_; ++ix)
    {
        for (var iy = 0; iy < this.model_.h_; ++iy)
        {
            fieldsToRefresh.push([ix, iy]);
        }
    }
    this.refresh(fieldsToRefresh);
}

var Logger = function(minSeverity)
{
    this.minSeverity_ = minSeverity;
}
Logger.debugSeverity = 0;
Logger.infoSeverity = 1;
Logger.logSeverity = 2;
Logger.warningSeverity = 3;
Logger.errorSeverity = 4;

Logger.prototype.debug = function(content)
{
    if (this.minSeverity_ > Logger.debugSeverity)
    {
        return;
    }
    console.debug.apply(console, arguments);
}

Logger.prototype.info = function(content)
{
    if (this.minSeverity_ > Logger.infoSeverity)
    {
        return;
    }
    console.info.apply(console, arguments);
}

Logger.prototype.log = function(content)
{
    if (this.minSeverity_ > Logger.logSeverity)
    {
        return;
    }
    console.log.apply(console, arguments);
}

Logger.prototype.warning = function(content)
{
    if (this.minSeverity_ > Logger.warningSeverity)
    {
        return;
    }
    console.warn.apply(console, arguments);
}

Logger.prototype.error = function(content)
{
    console.error.apply(console, arguments);
}

var debugMode = true;
var logger = new Logger(Logger.warningSeverity);
var model = new Model(logger);
var view = new View(model, debugMode, logger);
var controller = new Controller(model, view, logger);

setInterval(function(){controller.onTimerTick()}, 1000);

function isLeftButtonPressed(e)
{
    return 0 === e.button;
}

function isRightButtonPressed(e)
{
    return 2 === e.button;
}

function onMouseDownNew(e, x, y)
{
    if (null == e || null == e.button)
    {
        logger.error("Error! e or e.button is null");
        return;
    }
    if (isLeftButtonPressed(e))
    {
        controller.chooseField(x, y);
        return;
    }
    if (isRightButtonPressed(e))
    {
        controller.switchFlagIfPossible(x, y);
        return;
    }
}